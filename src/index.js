import { initList, renderList } from './color-table.js';
import './color-picker-element.js';

let colorListData = [
  {
    name: 'blue',
    type: 'main',
    color: '#0000ff',
  },
  {
    name: 'red',
    type: 'secondary',
    color: '#ff0000',
  },
  {
    name: 'green',
    type: 'accent',
    color: '#00ff00',
  },
];

const initColorListData = localStorage.getItem('color-list')
if(initColorListData) {
  const parsedColorListData = JSON.parse(initColorListData)
  if(parsedColorListData.length > 0) {
    colorListData = parsedColorListData
  }
}

export let colorListDataProxy = new Proxy(colorListData, {
  apply: function (target, thisArg, argumentsList) {
    return thisArg[target].apply(this, argumentList);
  },
  deleteProperty: function (target, property) {
    if(target.length < 2) {
      localStorage.removeItem('color-list')
    } else {
      localStorage.setItem('color-list', JSON.stringify(target))
    }
    return true;
  },
  set: function (target, property, value, receiver) {
    target[property] = value;
    renderList(target)
    delete target[property].edit
    localStorage.setItem('color-list', JSON.stringify(target))
    return true;
  },
});

initList(colorListDataProxy);
