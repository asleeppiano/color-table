import { html, render } from 'lit-html';
import { colorListDataProxy } from './index.js';
import { createPopper } from '@popperjs/core';
import { hsvToHex } from './utils.js';

const colorListElement = document.querySelector('.c-color-list');

let newitem = { name: '', type: '', color: '' };

function toggleControlButtons() {
  const addButton = document.querySelector('.js-add-button');
  addButton.classList.toggle('u-hidden');
  const confirmButton = document.querySelector('.js-confirm-button');
  confirmButton.classList.toggle('u-hidden');
  const cancelButton = document.querySelector('.js-cancel-button');
  cancelButton.classList.toggle('u-hidden');
}

function hideControlButtons() {
  const addButton = document.querySelector('.js-add-button');
  addButton.classList.add('u-hidden');
  const confirmButton = document.querySelector('.js-confirm-button');
  confirmButton.classList.add('u-hidden');
  const cancelButton = document.querySelector('.js-cancel-button');
  cancelButton.classList.add('u-hidden');
}

function showControlButtons() {
  const addButton = document.querySelector('.js-add-button');
  addButton.classList.remove('u-hidden');
  const confirmButton = document.querySelector('.js-confirm-button');
  confirmButton.classList.add('u-hidden');
  const cancelButton = document.querySelector('.js-cancel-button');
  cancelButton.classList.add('u-hidden');
}

function hideColorForm(colorForm) {
  if (!colorForm) {
    colorForm = document.querySelector('.js-color-form.js-color-form--editing');
    if(!colorForm) {
      return
    }
  }
  render('', colorForm);
  colorForm.classList.add('u-hidden');
  colorForm.classList.remove('js-color-form--editing');
}

function handleListItemEdit({target}) {
  const index = parseInt(target.closest('li').dataset.index)
  newitem.edit = false
  colorListDataProxy[index] = Object.assign({}, newitem)
  newitem = { name: '', type: '', color: '' };
  showControlButtons()
}
function handleListItemDelete({ target }) {
  const index = parseInt(target.closest('li').dataset.index)
  for (let i = 0; i < colorListDataProxy.length; i++) {
    colorListDataProxy[i].edit = false;
  }
  colorListDataProxy.splice(index, 1);
  newitem = { name: '', type: '', color: '' };
  showControlButtons()
}

function handleListItemAdd() {
  newitem = { name: '', type: '', color: '' }
  hideColorForm()
  const template = createNewItem(newitem);
  const colorForm = document.querySelector('.js-color-form');
  colorForm.classList.remove('u-hidden');
  colorForm.classList.add('js-color-form--editing');
  render(template, colorForm);
  toggleControlButtons();
  function handleDocKeypress(e) {
    if (e.key === 'Enter') {
      hideColorForm(colorForm);
      toggleControlButtons();
      colorListDataProxy.push(Object.assign({}, newitem));
      document.removeEventListener('keypress', handleDocKeypress);
    } else if (e.key === 'Esc') {
      hideColorForm();
      toggleControlButtons();
      document.removeEventListener('keypress', handleDocKeypress);
    }
  }
  document.addEventListener('keypress', handleDocKeypress);
  setTimeout(() => {
    window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    const nameInput = colorForm.querySelector('.c-color-item:first-of-type');
    nameInput.focus();
  }, 0);
}

function handleListItemCancel() {
  hideColorForm();
  toggleControlButtons();
  newitem = { name: '', type: '', color: '' };
}

function handleListItemConfirm() {
  hideColorForm();
  toggleControlButtons();
  colorListDataProxy.push(Object.assign({}, newitem));
  newitem = { name: '', type: '', color: '' };
}

function handleColorPick({ detail }) {
  newitem.color = `#${hsvToHex(
    detail.color.h,
    detail.color.s,
    detail.color.v,
  )}`;
  console.log('hello')
  const template = createNewItem(newitem);
  const colorForm = document.querySelector('.js-color-form.js-color-form--editing');
  render(template, colorForm);
  removeColorPicker()
}

function handleListItemChange() {
  for (let i = 0; i < colorListDataProxy.length; i++) {
    colorListDataProxy[i].edit = false;
  }
  const index = parseInt(this.dataset.index)
  colorListDataProxy[parseInt(index)] = {
    ...colorListDataProxy[parseInt(index)],
    edit: true,
  };
  hideColorForm()
  hideControlButtons()
  setTimeout(() => {
    const colorForm = document.querySelector('div.js-color-form')
    console.log('colorForm', colorForm)
    colorForm.classList.add('js-color-form--editing')
    const nameInput = colorForm.querySelector('.c-color-item:first-of-type');
    nameInput.focus();
  }, 0)
}


export function initList(colorListData, initColor) {
  if (!Array.isArray(colorListData)) {
    throw new TypeError('colorListData must be an array');
  }
  renderList(colorListData);
}

let dropdownInstance;

function removeColorPicker(e) {
  console.log('doc mousedown')
  const dropdown = document.querySelector('.c-dropdown');
  const colorPickerButton = document.querySelector('.c-color-picker-button');
  if (e && (dropdown.contains(e.target) || colorPickerButton.contains(e.target))) {
    return;
  }
  if (dropdownInstance) {
    dropdownInstance.destroy();
    dropdownInstance = null;
  }
  const colorPicker = document.querySelector('color-picker-element');
  colorPicker.removeAttribute('visible');
  dropdown.classList.add('u-hidden');
  colorPicker.removeEventListener('color-picked', handleColorPick);
  document.removeEventListener('mousedown', handleDocMouseDown);
}

function handleDocMouseDown(e) {
  removeColorPicker(e)
}

function handleColorPickerButton() {
  const colorPickerButton = document.querySelector('.c-color-picker-button');
  const dropdown = document.querySelector('.c-dropdown');
  dropdown.classList.remove('u-hidden');
  const colorPicker = document.querySelector('color-picker-element');
  colorPicker.setAttribute('visible', '');
  dropdownInstance = createPopper(colorPickerButton, dropdown, {
    modifiers: [
      {
        name: 'offset',
        options: {
          offset: [0, 20],
        },
      },
      {
        name: 'preventOverflow',
        options: {
          padding: 16,
        },
      },
    ],
  });
  setTimeout(() => {
    const colorPicker = document.querySelector('color-picker-element');
    colorPicker.addEventListener('color-picked', handleColorPick);
  });

  document.addEventListener('mousedown', handleDocMouseDown);
}

function handleTypeInput({ target }) {
  newitem.type = target.textContent;
}
function handleNameInput({ target }) {
  newitem.name = target.textContent;
}

function createNewItem(data) {
  if(data){
    newitem = Object.assign({}, data)
  }
  return html`<div
      contenteditable
      .innerHTML=${newitem.name}
      @input=${handleNameInput}
      class="c-color-item c-color-item--ci"
    ></div>
    <div
      contenteditable
      .innerHTML=${newitem.type}
      @input=${handleTypeInput}
      class="c-color-item c-color-item--ci"
    ></div>
    <div class="c-color-item">
      <button @click=${handleColorPickerButton} class="c-color-picker-button">
        ${newitem.color ? newitem.color : 'pick color'}
        <span style="background: ${newitem.color}" class="c-color-square"></span>
      </button>
    </div>
    <div class="c-dropdown u-hidden">
      <color-picker-element selectedcolor=${newitem.color ? newitem.color: ''} />
      <div class="c-arrow" data-popper-arrow></div>
    </div>`;
}

export function renderList(colorListData) {
  const ul = html`
    <li class="c-color-list__item c-color-list__item--header">
      <div class="c-color-item">name</div>
      <div class="c-color-item">type</div>
      <div class="c-color-item">color</div>
    </li>
    ${colorListData.map((colorData, i) => {
      if (colorData.edit) {
        return html` <li data-index=${i} class="u-mt--2">
          <div class="u-flex-1 js-color-form c-color-list__item c-color-list__item--edit">
            ${createNewItem(colorData)}
          </div>

          <div class="c-color-add">
            <button @click=${handleListItemEdit} class="c-button c-button--confirm">
              edit
            </button>
            <button @click=${handleListItemDelete} class="u-ml--8 c-button c-button--delete">
              delete
            </button>
          </div>
        </li>
        `;
      } else {
        return html`<li
          data-index=${i}
          @click=${handleListItemChange}
          class="c-color-list__item"
        >
          <div class="c-color-item">${colorData.name}</div>
          <div class="c-color-item">${colorData.type}</div>
          <div class="c-color-item">${colorData.color}<span style="background: ${colorData.color}" class="c-color-square"></span></div>
        </li>`;
      }
    })}
    <li
      class="c-color-list__item c-color-list__item--edit u-hidden js-color-form"
    ></li>
    <li class="c-color-add">
      <button class="c-button c-button--add js-add-button" @click=${handleListItemAdd}>
        add
      </button>
      <button
        class="c-button c-button--confirm u-hidden js-confirm-button"
        @click=${handleListItemConfirm}
      >
        confirm
      </button>
      <button
        class="u-ml--8 c-button c-button--delete u-hidden js-cancel-button"
        @click=${handleListItemCancel}
      >
        cancel
      </button>
    </li>
  `;
  render(ul, colorListElement);
}
