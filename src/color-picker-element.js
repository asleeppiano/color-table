import { LitElement, html, css } from 'lit-element';
import { hsvToHsl, hexToHsv, hsvToHex, hsvToRgb, rgbToHsv } from './utils.js';
import debounce from 'lodash-es/debounce'

class ColorPickerElement extends LitElement {
  static get styles() {
    return css`
      :host {
      }
      .wrapper {
        border: 1px solid black;
        padding: 1.5rem;
        background: lightgray;
        box-shadow: 4px 4px 1px hsla(0, 0%, 0%, 0.25);
      }
      .color-picker {
        position: relative;
        width: 200px;
        height: 150px;
        border: 1px solid black;
      }
      @media (min-width: 640px) {
        .color-picker {
          width: 300px;
          height: 200px;
        }
      }
      .color-picker__palette {
        width: 100%;
        height: 100%;
      }
      .color-picker__cursor {
        width: 12px;
        height: 12px;
        border-radius: 100%;
        background: transparent;
        border: 3px solid rgba(255, 255, 255, 0.7);
        position: absolute;
        left: 50%;
        top: 50%;
      }
      .color-picker__slider {
        position: relative;
        width: 10px;
        height: 16px;
        background: transparent;
        border: 2px solid black;
      }
      .color-picker__display {
        width: 50px;
        height: 50px;
        border: 1px solid black;
        border-radius: 6px;
      }
      .color-picker__hue {
        border: 1px solid black;
        width: 200px;
        height: 20px;
        background: linear-gradient(
          90deg,
          #f00,
          #ff0,
          #0f0,
          #0ff,
          #00f,
          #f0f,
          #f00
        );
      }
      @media (min-width: 640px) {
        .color-picker__hue {
          width: 300px;
        }
      }
      .color-picker__bottom {
        padding-top: 1rem;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
      }
      @media (min-width: 640px) {
        .color-picker__bottom {
          flex-direction: row;
        }
      }
      .pick-button {
        width: 70px;
        cursor: pointer;
        background: transparent;
        border: 1px solid black;
        border-radius: 6px;
        padding: 0.2rem 0.4rem;
        margin-top: 1rem;
      }
      @media (min-width: 640px) {
        .pick-button {
          margin-left: 1rem;
        }
      }
      .pick-button:hover {
        background-color: hsla(0, 0%, 90%, 0.8);
      }
      .toggle-button-wrapper {
        margin-bottom: 0.5rem;
      }
      .color-picker-input {
        width: 142px;
        margin-top:1rem;
      }
      @media (min-width: 640px) {
        .color-picker-input {
          margin-left:1rem;
        }
      }
      .toggle-button {
        cursor:pointer;
        background: transparent;
        border: none;
        padding: 0.2rem 0.4rem;
      }
      .toggle-button:nth-of-type(2) {
        margin-top: 0.5rem;
      }
      @media (min-width: 640px) {
        .toggle-button:nth-of-type(2) {
          margin-left: 0.5rem;
        }
      }
      .toggle-button--active {
         background: lightgreen;
      }
      .input {
        display: none;
      }
      .input--active {
        display: flex;
      }
      .small-input {
        width: 30px;
      }
      .big-input {
        width: 60px;
      }
    `;
  }

  static get properties() {
    return {
      selectedColor: { type: String },
      visible: { type: Boolean },
    };
  }

  setColor(color) {
    this.paletteStyle = `linear-gradient(to top, rgba(0, 0, 0, ${color.a}), transparent),
    linear-gradient(to left, hsla(${color.h}, 100%, 50%, ${color.a}), hsla(100, 100%, 100%, ${color.a}))`;
  }

  setColorBoxStyle(color) {
    const hsl = hsvToHsl(this.color.h, this.color.s, this.color.v);
    this.color.colorBoxStyle = `hsla(${hsl.h.toFixed(2)}, ${hsl.s.toFixed(
      2,
    )}%, ${hsl.l.toFixed(2)}%, ${color.a});`;
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      if (propName === 'selectedColor' && this.selectedColor) {
        const color = this.selectedColor.substring(1);
        const { h, s, v } = hexToHsv(color);
        const hsl = hsvToHsl(h, s, v);
        this.color = {
          h,
          s,
          v,
          a: 1,
          colorBoxStyle: `hsla(${hsl.h.toFixed(2)}, ${hsl.s.toFixed(
            2,
          )}%, ${hsl.l.toFixed(2)}%, 1)`,
        };
        this.requestUpdate();
      } else if (propName === 'visible' && this.visible) {
        setTimeout(() => {
          this.setColor(this.color);
          this.setColorBoxStyle(this.color);
          this.updateCursorFromColor(this.color);
          this.updateHueFromColor(this.color);
          this.requestUpdate();
        }, 0);
      } else if (propName === 'visible' && !this.visible) {
        document.removeEventListener('mousemove', this.handlePaletteMouseMove);
        document.removeEventListener('touchmove', this.handlePaletteMouseMove);
        document.removeEventListener('mousemove', this.handleSliderMouseMove);
        document.removeEventListener('touchmove', this.handleSliderMouseMove);
        const color = this.selectedColor.substring(1);
        const { h, s, v } = hexToHsv(color);
        const hsl = hsvToHsl(h, s, v);
        this.color = {
          h,
          s,
          v,
          a: 1,
          colorBoxStyle: `hsla(${hsl.h.toFixed(2)}, ${hsl.s.toFixed(
            2,
          )}%, ${hsl.l.toFixed(2)}%, 1)`,
        };
      }
    });
  }

  constructor() {
    super();
    this.selectedColor = '#da5bb1';
    const color = this.selectedColor.substring(1);
    const { h, s, v } = hexToHsv(color);
    const hsl = hsvToHsl(h, s, v);
    this.color = {
      h,
      s,
      v,
      a: 1,
      colorBoxStyle: `hsla(${hsl.h}, ${hsl.s}%, ${hsl.l}%, 1)`,
    };
    this.handlePaletteMouseMove = this.handlePaletteMouseMove.bind(this);
    this.handleSliderMouseMove = this.handleSliderMouseMove.bind(this);
    this.debouncedRgbInput = debounce(this.handleRgbInput.bind(this), 180)
    this.setColor(this.color);
  }

  updateCursorFromColor(color) {
    this.paletteBCR = this.palette.getBoundingClientRect();
    const x = (color.s / 100) * this.paletteBCR.width;
    const y = ((100 - color.v) / 100) * this.paletteBCR.height;

    this.cursor.style.left = `calc(${(
      (x / this.paletteBCR.width) *
      100
    ).toFixed(2)}% - ${this.cursor.offsetWidth / 2}px)`;

    this.cursor.style.top = `calc(${(
      (y / this.paletteBCR.height) *
      100
    ).toFixed(2)}% - ${this.cursor.offsetHeight / 2}px)`;
  }

  async firstUpdated() {
    await new Promise((r) => setTimeout(r, 0));
    this.palette = this.shadowRoot.querySelector('.color-picker');
    this.cursor = this.shadowRoot.querySelector('.color-picker__cursor');
    this.hue = this.shadowRoot.querySelector('.color-picker__hue');
    this.slider = this.shadowRoot.querySelector('.color-picker__slider');
    this.currentInput = this.shadowRoot.querySelector('.toggle-button--active')

    this.paletteBCR = this.palette.getBoundingClientRect();

    const x = (this.color.s / 100) * 300;
    const y = ((100 - this.color.v) / 100) * 200;

    this.cursor.style.left = `calc(${(
      (x / this.paletteBCR.width) *
      100
    ).toFixed(2)}% - ${this.cursor.offsetWidth / 2}px)`;

    this.cursor.style.top = `calc(${(
      (y / this.paletteBCR.height) *
      100
    ).toFixed(2)}% - ${this.cursor.offsetHeight / 2}px)`;
  }

  handlePaletteMouseDown() {
    // auto bound
    document.addEventListener('mousemove', this.handlePaletteMouseMove);
    document.addEventListener('touchmove', this.handlePaletteMouseMove);
  }

  handlePaletteMouseUp() {
    // auto bound
    document.removeEventListener('mousemove', this.handlePaletteMouseMove);
    document.removeEventListener('touchmove', this.handlePaletteMouseMove);
  }

  updateCursor(e) {
    this.paletteBCR = this.palette.getBoundingClientRect();

    const touch = e && e.touches && e.touches[0];
    let x = e ? (touch || e).clientX : 0;
    let y = e ? (touch || e).clientY : 0;

    if (x < this.paletteBCR.left) {
      x = this.paletteBCR.left;
    } else if (x > this.paletteBCR.left + this.paletteBCR.width) {
      x = this.paletteBCR.left + this.paletteBCR.width;
    }
    if (y < this.paletteBCR.top) {
      y = this.paletteBCR.top;
    } else if (y > this.paletteBCR.top + this.paletteBCR.height) {
      y = this.paletteBCR.top + this.paletteBCR.height;
    }

    // Normalize
    x -= this.paletteBCR.left;
    y -= this.paletteBCR.top;

    this.cursor.style.left = `calc(${(
      (x / this.paletteBCR.width) *
      100
    ).toFixed(2)}% - ${this.cursor.offsetWidth / 2}px)`;

    this.cursor.style.top = `calc(${(
      (y / this.paletteBCR.height) *
      100
    ).toFixed(2)}% - ${this.cursor.offsetHeight / 2}px)`;

    const cx = this.clamp(x / this.paletteBCR.width);
    const cy = this.clamp(y / this.paletteBCR.height);
    return { cx, cy };
  }

  clamp = (v) => Math.max(Math.min(v, 1), 0);

  updateColor(x, y) {
    this.color.s = (x * 100).toFixed(2);
    this.color.v = (100 - y * 100).toFixed(2);
    this.setColorBoxStyle(this.color);
    let colorUpdatedEvent = new CustomEvent('color-updated', {
      detail: {
        color: this.color,
      },
    });
    this.dispatchEvent(colorUpdatedEvent);
    this.requestUpdate();
  }

  handlePaletteMouseMove(e) {
    const { cx, cy } = this.updateCursor(e);
    this.updateColor(cx, cy);
  }

  handleSliderMouseDown() {
    document.addEventListener('mousemove', this.handleSliderMouseMove);
    document.addEventListener('touchmove', this.handleSliderMouseMove);
  }

  handleSliderMouseUp() {
    document.removeEventListener('mousemove', this.handleSliderMouseMove);
    document.removeEventListener('touchmove', this.handleSliderMouseMove);
  }

  handleSliderMouseMove(e) {
    this.hueBCR = this.hue.getBoundingClientRect();

    const touch = e && e.touches && e.touches[0];
    let x = e ? (touch || e).clientX : 0;

    if (x < this.hueBCR.left) {
      x = this.hueBCR.left;
    } else if (x > this.hueBCR.left + this.hueBCR.width) {
      x = this.hueBCR.left + this.hueBCR.width;
    }

    x -= this.hueBCR.left;

    this.slider.style.left = `calc(${(x / this.hueBCR.width) * 100}% - ${this.slider.offsetWidth / 2}px)`;

    const cx = this.clamp(x / this.hueBCR.width);
    this.setHue(cx);
  }

  updateHueFromColor(color) {
    this.hueBCR = this.hue.getBoundingClientRect();
    const x = (color.h * 100) / 360;
    this.slider.style.left = `calc(${x}% - ${this.slider.offsetWidth / 2}px)`;
  }

  setHue(x) {
    this.color.h = Number(Math.trunc(x * 360));
    this.setColor(this.color);
    this.setColorBoxStyle(this.color);
    let colorUpdatedEvent = new CustomEvent('color-updated', {
      detail: {
        color: this.color,
      },
    });
    this.dispatchEvent(colorUpdatedEvent);
    this.requestUpdate();
  }

  pickColor() {
    let pickEvent = new CustomEvent('color-picked', {
      detail: {
        color: this.color,
      },
    });
    this.dispatchEvent(pickEvent);
  }

  handleHexInput({target}) {
    let hexValue = target.value
    if(hexValue[0] === '#') {
      hexValue = hexValue.substring(1)
    }
    if(hexValue.length < 6 || !/^[0-9A-F]{6}$/i.test(hexValue)) {
      return
    }
    const {h, s, v} = hexToHsv(hexValue)
    this.fullUpdate(h, s, v)
  }

  handleRgbInput() {
    const redInput = this.shadowRoot.querySelector('input[name="red"]')
    const blueInput = this.shadowRoot.querySelector('input[name="blue"]')
    const greenInput = this.shadowRoot.querySelector('input[name="green"]')
    if(parseInt(redInput.value, 10) >= 0 && parseInt(redInput.value, 10) < 256 &&
parseInt(greenInput.value, 10) >= 0 && parseInt(greenInput.value, 10) < 256 &&
parseInt(blueInput.value, 10) >= 0 && parseInt(blueInput.value, 10) < 256) {
      const {h, s, v} = rgbToHsv(parseInt(redInput.value, 10), parseInt(greenInput.value, 10), parseInt(blueInput.value, 10))
      this.fullUpdate(h, s, v)
    }
  }

  fullUpdate(h, s, v) {
    const hsl = hsvToHsl(h, s, v)
    this.color = {
      h,
      s,
      v,
      a: 1,
      colorBoxStyle: `hsla(${hsl.h.toFixed(2)}, ${hsl.s.toFixed(
        2,
      )}%, ${hsl.l.toFixed(2)}%, 1)`,
    };
    this.setColor(this.color)
    this.setColorBoxStyle(this.color)
    this.updateCursorFromColor(this.color)
    this.updateHueFromColor(this.color)
    this.requestUpdate()
  }

  selectInput({target}) {
    const buttons = this.shadowRoot.querySelectorAll('.toggle-button')
    buttons.forEach(button => {
      if(button.getAttribute('value') === target.getAttribute('value')) {
        button.classList.add('toggle-button--active')
      } else {
        button.classList.remove('toggle-button--active')
      }
    })
    this.currentInput = this.shadowRoot.querySelector('.toggle-button--active')
    this.requestUpdate()
  }

  render() {
    return html`
      <div class="wrapper">
        <div
          @mousedown=${this.handlePaletteMouseDown}
          @mouseup=${this.handlePaletteMouseUp}
          @touchstart=${this.handlePaletteMouseDown}
          @touchend=${this.handlePaletteMouseUp}
          @touchcancel=${this.handlePaletteMouseUp}
          class="color-picker"
        >
          <div
            style="background: ${this.paletteStyle}"
            class="color-picker__palette"
          ></div>
          <div class="color-picker__cursor"></div>
        </div>
        <div
          @mousedown=${this.handleSliderMouseDown}
          @mouseup=${this.handleSliderMouseUp}
          @touchstart=${this.handleSliderMouseDown}
          @touchend=${this.handleSliderMouseUp}
          @touchcancel=${this.handleSliderMouseUp}
          class="color-picker__hue"
        >
          <div class="color-picker__slider"></div>
        </div>
        <div class="color-picker__bottom">
          <div
            style="background: ${this.color.colorBoxStyle}"
            class="color-picker__display"
          ></div>
          <div class="color-picker-input">
            <div class="toggle-button-wrapper">
              <button @click=${this.selectInput} value="hex" class="toggle-button toggle-button--active" type="button">
                hex
              </button>
              <button @click=${this.selectInput} value="rgb" class="toggle-button" type="button">rgb</button>
            </div>
            <div data-name="hex" class="input input--hex ${this.currentInput && this.currentInput.getAttribute('value') === 'hex' ? 'input--active' : ''}">
              <input
                class="big-input"
                @input=${this.handleHexInput}
                .value=${hsvToHex(this.color.h, this.color.s, this.color.v)}
                data-name="hex"
                type="text"
              />
            </div>
            <div data-name="rgb" class="input input--rgb ${this.currentInput && this.currentInput.getAttribute('value') === 'rgb' ? 'input--active' : ''}">
              <input
                @input=${this.debouncedRgbInput}
                .value=${Math.round(hsvToRgb(this.color.h, this.color.s, this.color.v).r)}
                name="red"
                class="small-input input--red"
                type="text"
              />
              <input
                @input=${this.debouncedRgbInput}
                .value=${Math.round(hsvToRgb(this.color.h, this.color.s, this.color.v).g)}
                name="green"
                class="small-input input--green"
                type="text"
              />
              <input
                @input=${this.debouncedRgbInput}
                .value=${Math.round(hsvToRgb(this.color.h, this.color.s, this.color.v).b)}
                name="blue"
                class="small-input input--blue"
                type="text"
              />
            </div>
          </div>
          <button type="button" @click=${this.pickColor} class="pick-button">
            pick<br/>${hsvToHex(this.color.h, this.color.s, this.color.v)}
          </button>
        </div>
      </div>
    `;
  }
}

customElements.define('color-picker-element', ColorPickerElement);
